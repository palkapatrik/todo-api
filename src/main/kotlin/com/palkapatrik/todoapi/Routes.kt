package com.palkapatrik.todoapi

object Routes {
    const val TASKS = "/api/tasks"
    const val LISTS = "/api/lists"
}
