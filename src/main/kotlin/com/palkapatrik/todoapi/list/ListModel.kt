package com.palkapatrik.todoapi.list

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class ListModel(@Id val id: String,
                val name: String,
                val archived: Boolean) {
    constructor() : this("", "", false)
}
