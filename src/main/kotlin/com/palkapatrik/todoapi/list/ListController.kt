package com.palkapatrik.todoapi.list

import com.palkapatrik.todoapi.CommonHelpers
import com.palkapatrik.todoapi.Routes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@RestController
@RequestMapping(Routes.LISTS)
class ListController {
    @Autowired
    lateinit var listRepository: ListRepository

    @GetMapping("/")
    fun getLists(): Iterable<ListModel> = listRepository.findAll()

    @GetMapping("/archived/")
    fun getArchivedLists() = listRepository.finAllArchived()

    @GetMapping("/{id}")
    fun getList(@PathVariable id: String) = listRepository.findById(id)

    @PostMapping("/create")
    fun createList(@RequestBody body: ListModel): ResponseEntity<Unit> {
        if (listValidator(body)) {
            if (!listRepository.existsById(body.id)) {
                listRepository.save(body)
                return CommonHelpers.buildResponse(HttpStatus.CREATED)
            }
        }
        return CommonHelpers.buildResponse(HttpStatus.NOT_ACCEPTABLE)
    }

    @DeleteMapping("/delete/{id}")
    fun deleteList(@PathVariable id: String): ResponseEntity<Unit> {
        if (listRepository.existsById(id)) {
            listRepository.deleteById(id)
            return CommonHelpers.buildResponse(HttpStatus.OK)
        }
        return CommonHelpers.buildResponse(HttpStatus.NOT_FOUND)
    }

    @PutMapping("/rename/{id}")
    fun renameList(@PathVariable id: String, @RequestBody name: String): ResponseEntity<Unit> {
        if (name.isEmpty() || !listRepository.existsById(id))
            return CommonHelpers.buildResponse(HttpStatus.BAD_REQUEST)
        listRepository.updateName(id, name)
        return CommonHelpers.buildResponse(HttpStatus.OK)
    }

    @PutMapping("/archive/{id}")
    fun archiveList(@PathVariable id: String, @RequestBody archived: String): ResponseEntity<Unit> {
        if (!listRepository.existsById(id))
            return CommonHelpers.buildResponse(HttpStatus.BAD_REQUEST)
        listRepository.toggleArchived(id, archived.toBoolean())
        return CommonHelpers.buildResponse(HttpStatus.OK)
    }

    fun listValidator(list: ListModel): Boolean {
        println("validating list: ${list.id}, ${list.name}, ${list.archived}")
        try {
            checkNotNull(list.id)
            checkNotNull(list.name)
            checkNotNull(list.archived)
        } catch (e: Exception) {
            return false
        }
        return (list.id.isNotEmpty() && list.name.isNotEmpty())
    }
}
