package com.palkapatrik.todoapi.list

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface ListRepository: JpaRepository<ListModel, String> {
    @Query("SELECT l FROM ListModel l WHERE l.archived = TRUE")
    fun finAllArchived(): Iterable<ListModel>

    @Transactional
    @Modifying
    @Query("UPDATE ListModel l SET l.name = :name WHERE l.id = :id")
    fun updateName(@Param("id") id: String, @Param("name") name: String)

    @Transactional
    @Modifying
    @Query("UPDATE ListModel l SET l.archived = :archived WHERE l.id = :id")
    fun toggleArchived(@Param("id") id: String, @Param("archived") archived: Boolean)
}
