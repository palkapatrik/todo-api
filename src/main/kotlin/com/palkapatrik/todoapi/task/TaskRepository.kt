package com.palkapatrik.todoapi.task

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface TaskRepository: JpaRepository<TaskModel, String> {
    @Query("SELECT t FROM TaskModel t WHERE t.listId = :listId")
    fun findAllByListId(@Param("listId") listId: String): Iterable<TaskModel>
}
