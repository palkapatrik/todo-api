package com.palkapatrik.todoapi.task

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class TaskModel(val value: String,
                val listId: String,
                val completed: Boolean,
                @Id val id: String) {
    constructor(): this("", "", false, "")
}
