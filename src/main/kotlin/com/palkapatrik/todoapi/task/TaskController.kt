package com.palkapatrik.todoapi.task

import com.palkapatrik.todoapi.CommonHelpers
import com.palkapatrik.todoapi.Routes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@RestController
@RequestMapping(Routes.TASKS)
class TaskController {
    @Autowired
    lateinit var repository: TaskRepository

    @GetMapping("/{listId}")
    fun getTask(@PathVariable listId: String) = repository.findAllByListId(listId)

    @PostMapping("/{listId}/add")
    fun addTask(@PathVariable listId: String, @RequestBody body: TaskModel): ResponseEntity <Unit> {
        if (taskValidator(body))
            if (!repository.existsById(body.id)) {
                repository.saveAndFlush(body)
                return CommonHelpers.buildResponse(HttpStatus.CREATED)
            }
        return CommonHelpers.buildResponse(HttpStatus.NOT_ACCEPTABLE)
    }

    fun taskValidator(task: TaskModel): Boolean {
        println("validating task: ${task.id}, ${task.value}, ${task.listId}, ${task.completed}")
        try {
            checkNotNull(task.id)
            checkNotNull(task.value)
            checkNotNull(task.listId)
            checkNotNull(task.completed)
        } catch (e: Exception) {
            return false
        }
        return task.id.isNotEmpty() && task.value.isNotEmpty()
    }

}

