package com.palkapatrik.todoapi

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

object CommonHelpers {
    fun buildResponse(status: HttpStatus): ResponseEntity<Unit> {
        return ResponseEntity.status(status).build()
    }
}
